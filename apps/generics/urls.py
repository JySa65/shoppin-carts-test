from django.urls import path
# views
from apps.generics.views import IndexTemplateView, LogoutView, LoginView, SignupView

app_name = "generics"

urlpatterns = [
    path('', IndexTemplateView.as_view(), name="index"),
    path('login/', LoginView.as_view(), name="login"),
    path('signup/', SignupView.as_view(), name="signup"),
    path('logout/', LogoutView.as_view(), name="logout")
]
