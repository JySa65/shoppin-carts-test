from django.db.models import Count
from django.views.generic import TemplateView, View, FormView, CreateView
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib.auth import (
    login as auth_login,
    logout as auth_logout,
    update_session_auth_hash,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator


# app generics
from apps.accounts.models import User
from apps.generics.forms import AuthenticationForm, UserCreationForm
# models courses
from apps.courses.models import Cours
from apps.carts.models import ShoppinCart


class IndexTemplateView(TemplateView):
    """Esta vista retorna los 10 curso mas comprados"""
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        last_10_more_course = ShoppinCart.objects.filter(state=True).values('courses__name').annotate(
            count_course=Count('courses__pk')).order_by('-count_course').values_list('courses__pk', flat=True)[:10]
        courses = []
        for _id in last_10_more_course:
            try:
                courses.append(Cours.objects.get(pk=_id))
            except:
                continue
        context["courses"] = courses
        return context


class LoginView(FormView):
    """
    Display the login form and handle the login action.
    """
    form_class = AuthenticationForm
    template_name = 'generics/login.html'
    redirect_authenticated_user = True

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if self.redirect_authenticated_user and self.request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        url_next = self.request.GET.get('next')
        if url_next is not None:
            return url_next
        return reverse_lazy('courses:course_list')

    def form_valid(self, form):
        """Security check complete. Log the user in."""
        auth_login(self.request, form.get_user())
        self.object = form.get_user()
        return HttpResponseRedirect(self.get_success_url())


class SignupView(CreateView):
    """ Display the signup form and handle the signin action. """
    model = User
    form_class = UserCreationForm
    template_name = 'generics/signup.html'
    success_url = reverse_lazy('courses:course_list')

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse_lazy('courses:course_list'))
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        auth_login(self.request, self.object)
        return HttpResponseRedirect(self.get_success_url())


class LogoutView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return HttpResponseRedirect(reverse_lazy("generics:index"))
