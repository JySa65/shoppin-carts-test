from django.contrib import admin
# models
from apps.courses.models import Cours, Category
# Register your models here.


@admin.register(Cours)
class CoursesAdmin(admin.ModelAdmin):
    list_display = ('name', 'summary', 'created_at',
                    'get_categorys', 'price', 'state')
    search_fields = ('name', 'summary')
    filter_horizontal = ('categorys',)
    list_editable = ('state',)

    def get_categorys(self, instance):
        return ", ".join([p.title for p in instance.categorys.all()])
    get_categorys.short_description = 'Categorias'


admin.site.register(Category)
