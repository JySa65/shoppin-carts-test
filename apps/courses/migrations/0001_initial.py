# Generated by Django 3.0.7 on 2020-06-10 12:26

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150)),
                ('slug', models.SlugField(editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='Cours',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('slug', models.SlugField(editable=False, max_length=150, unique=True)),
                ('summary', models.TextField(max_length=250, null=True)),
                ('cover_page', models.ImageField(null=True, upload_to='baners_cursos')),
                ('duration', models.IntegerField()),
                ('state', models.BooleanField(default=True)),
                ('is_free', models.BooleanField(default=False)),
                ('price', models.DecimalField(decimal_places=2, default=0.0, max_digits=5)),
                ('discount', models.DecimalField(decimal_places=2, default=0.0, max_digits=5)),
                ('about', models.TextField(max_length=2000, null=True)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('authors', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
                ('categorys', models.ManyToManyField(to='courses.Category')),
            ],
        ),
    ]
