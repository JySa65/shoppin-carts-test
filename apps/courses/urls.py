from django.urls import path
# views
from apps.courses.views import CourseListview, CourseDetailview

app_name = "courses"

urlpatterns = [
    path('', CourseListview.as_view(), name="course_list"),
    path('<slug:slug>/detail/', CourseDetailview.as_view(), name="course_detail"),
]
