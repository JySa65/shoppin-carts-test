from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
# model course
from apps.courses.models import Cours
# Create your views here.


class CourseListview(ListView):
    model = Cours
    queryset = Cours.objects.filter(state=True)


class CourseDetailview(DetailView):
    model = Cours
    pk_url_kwarg = 'slug'

    def get_object(self, queryset=None):
        return get_object_or_404(self.model, slug=self.kwargs.get('slug'))
