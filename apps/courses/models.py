from django.db import models
from django.template.defaultfilters import slugify
# models accounts
from apps.accounts.models import User

# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=150)
    slug = models.SlugField(editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class Cours(models.Model):
    name = models.CharField(max_length=250)
    slug = models.SlugField(editable=False, max_length=150, unique=True)
    summary = models.TextField(max_length=250, null=True)
    cover_page = models.ImageField(
        upload_to='baners_cursos', null=True)
    duration = models.IntegerField()
    categorys = models.ManyToManyField(Category)
    state = models.BooleanField(default=True)
    is_free = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    discount = models.DecimalField(
        max_digits=5, decimal_places=2, default=0.00)
    about = models.TextField(max_length=2000, null=True)
    authors = models.ManyToManyField(User)

    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Cours, self).save(*args, **kwargs)
