from django.db.models import Sum, Q
from django import template
# models accounts
from apps.accounts.models import User
# models carts
from apps.carts.models import ShoppinCart

register = template.Library()


@register.filter(name="courses")
def courses(user):
    return ShoppinCart.objects.filter(owner=user, state=True).first()


@register.filter(name='total_course')
def total_course(user):
    try:
        total = user.shoppincart.courses.filter(state=True).aggregate(
            total=Sum('price', filter=Q(is_free=False, state=True)))
        return total.get('total') if total.get('total') else 0
    except ShoppinCart.DoesNotExist:
        return 0


@register.filter(name='user_has_course')
def user_has_course(course, user):
    if user.is_anonymous:
        return False
    return ShoppinCart.objects.filter(owner=user, courses=course).exists()


@register.filter(name='user_has_total_course')
def user_has_total_course(user):
    if user.is_anonymous:
        return 0
    shoppin = ShoppinCart.objects.filter(owner=user)
    if shoppin.exists():
        return shoppin.first().courses.filter(state=True).count()
    return 0
