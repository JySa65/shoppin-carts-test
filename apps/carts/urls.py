from django.urls import path
# views
from apps.carts.views import AddToCartView, RemoveFromCartView, ShoppinCartListView

app_name = "carts"

urlpatterns = [
    path('shopping-carts/', ShoppinCartListView.as_view(), name="shopping_cart_list"),
    path('add-to-cart/<slug:slug>/', AddToCartView.as_view(), name="add_to_cart"),
    path('remove-cart/<slug:slug>/', RemoveFromCartView.as_view(), name="remove_from_cart")
]