from django.views.generic import View, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
# models shoppibcart
from apps.carts.models import ShoppinCart
# models courses
from apps.courses.models import Cours

# Create your views here.


class AddToCartView(LoginRequiredMixin, View):
    model = Cours
    pk_url_kwarg = 'slug'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        course = get_object_or_404(self.model, slug=self.kwargs.get('slug'))
        shoppint, _ = ShoppinCart.objects.get_or_create(owner=request.user)
        shoppint.courses.add(course)
        messages.add_message(request, messages.INFO,
                             f'{course.name} añadido al carrito')
        return HttpResponseRedirect(reverse_lazy('courses:course_detail', args=(course.slug,)))


class RemoveFromCartView(LoginRequiredMixin, View):
    model = Cours
    pk_url_kwarg = 'slug'

    def post(self, request, *args, **kwargs):
        course = get_object_or_404(self.model, slug=self.kwargs.get('slug'))
        shoppint, _ = ShoppinCart.objects.get_or_create(owner=request.user)
        shoppint.courses.remove(course)
        messages.add_message(request, messages.INFO,
                             f'{course.name} removido del carrito')
        return HttpResponseRedirect(reverse_lazy('courses:course_list'))


class ShoppinCartListView(LoginRequiredMixin, ListView):
    template_name = "carts/shoppincart_list.html"
    model = ShoppinCart

    def get_queryset(self):
        shopping = self.model.objects.filter(owner=self.request.user)
        if shopping.exists():
            return shopping.first().courses.all()
        return []
