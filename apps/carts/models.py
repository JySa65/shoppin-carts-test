from django.db import models
# model user
from apps.accounts.models import User
# model courses
from apps.courses.models import Cours

# Create your models here.


class ShoppinCart(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE)
    courses = models.ManyToManyField(Cours, blank=True)
    state = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.owner.username
