from django.contrib import admin
from django.db.models import Sum
# models
from apps.carts.models import ShoppinCart
# Register your models here.


@admin.register(ShoppinCart)
class ShoppinCartAdmin(admin.ModelAdmin):

    list_display = ('owner', 'get_courses', 'created_at', 'get_total', 'state')
    search_fields = ('owner__username', 'courses__name')
    filter_horizontal = ('courses',)

    def get_courses(self, instance):
        return ", ".join([p.name for p in instance.courses.all()])
    get_courses.short_description = 'Cursos'

    def get_total(self, instance):
        total = instance.courses.aggregate(total=Sum('price'))
        return total.get('total', 0)
    get_total.short_description = 'Precio total Compra'
