from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

# models
from apps.accounts.models import User

# Register your models here.
@admin.register(User)
class UserAdmin(BaseUserAdmin):
    list_display = ('username', 'email', 'date_joined')
    fieldsets = (
        (None, {'fields': (
            'username',
            'password'
        )}),
        (_('Personal info'), { 'fields': ('email',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions', )}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username',
                'email',
                'password1',
                'password2'
            ),
        }),
    )
    list_filter = ('is_staff', 'is_superuser', 'is_active',)
    search_fields = ('email', 'username')
    ordering = ('-date_joined',)
