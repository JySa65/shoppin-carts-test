# Shoppin Carts

## Requirements

- Docker
- docker-compose

## Sen Env

```sh
$ copy .env.example .env
$ nano .env
```

## Run Project

```sh
$ docker-compose build
$ docker-compose --rm web python manage.py migrate
$ docker-compose --rm web python manage.py createsuperuser
$ docker-compose up
```

## Down Project

```sh
$ docker-compose down
```

## Endpoint

- http://localhost:8000
