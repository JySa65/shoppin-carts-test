FROM python:3.8
ENV PYTHONUNBUFFERED 1
WORKDIR /webapp
COPY requirements.txt /webapp/
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY . .